$(function () {
    var AMOUNT_TO_LOAD = 20;

    var currentGenreFilter;
    var currentGenderFilter;

    var books = [];
    var offset = 0;

    var endReached = false;

    var genres = ["Finance/Friday", "Horror/Halloween", "Horror", "Drama", "Romance", "Sci-Fi", "Non-Fiction", "Self-Help", "Fantasy", "Finance",
        "Adventure", "Thriller", "Crime", "Biography", "Education", "Satire", "Mistery", "Action", "Health",
        "Guide", "Math", "Art", "Science", "Diaries", "Religion", "Autobiography"
    ];
    genres.sort();


    var win = $(window);
    // Each time the user scrolls
    win.scroll(function () {

        var height = win.scrollTop();
        if (height > 100) {
            $('#back2Top').fadeIn();
        } else {
            $('#back2Top').fadeOut();
        }

        // End of the document reached?
        if ($(document).height() - win.height() == win.scrollTop()) {

            if (!endReached) {
                $("#load-more").html("Loading More...");

                // setTimeout simulates asking database for information
                setTimeout(function () {
                    $("#load-more").html("Load 20 More");
                    loadRegular();
                }, 800);
            }
        }
    });

    $("#back2Top").click(function(event) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

    // not crucial, just for fun
    var intervalController = setInterval(function () {
        changeText()
    }, 3000);
    var loadingTexts = ["Finding books...", "BOO, I just found a few horror books!", "I'll skip those Twilight books", "Hold on, almost there!", "Lots of books, might take a while...", "OK, almost there...", "Few more seconds...", "Don't loose your patience!", "Here they are..."];
    var textIndex = 0;

    /** 
     * Changes text of #loading-text while loading
     */
    function changeText() {
        $("#loading-text").html(loadingTexts[getRandomInt(loadingTexts.length)]);
    }
    startLoading();
    changeText();
    addItemsToNav();

    // gets json from files
    $.ajax({
        url: 'files/json/books.json',
        dataType: 'json',
        success: function (data) {
            books = data.data;
            stopLoading();
            clearInterval(intervalController);
            reload();
        }
    });

    /**
     * #load-moer click function
     */
    $("#load-more").click(function () {
        loadRegular();
    });

    var lastOffset = 0;
    /**
     * Loads a more books to the #book-container
     * 
     * @param {int} a 
     * @param {string} genderFilter 
     * @param {string} genreFilter 
     * @param {boolean} loadAgain 
     */
    function loadMore(a, genderFilter, genreFilter, loadAgain) {
        if (loadAgain) {
            offset = 0;
            $("#book-container").html("");
        }
        console.log(offset);
        for (let i = offset; i < offset + a; i++) {
            if (i >= books.length) {
                endReached = true;
                $("#load-more").off('click');
                $("#load-more").html("no more");
                return;
            }

            if (!isBlank(genderFilter) && books[i].author.sex != genderFilter) {
                a++;
                continue;
            }

            if (genreFilter == "Horror/Halloween") {
                if ((!arrayContains("Horror", books[i].genres)) || books[i].published.month != 10 || books[i].published.day != 31) {
                    a++;
                    continue;
                }
            } else if (genreFilter == "Finance/Friday") {
                let date = new Date(books[i].published.year, books[i].published.month - 1, books[i].published.day);

                if (!(lastFriday(books[i].published.year, books[i].published.month) == date.toDateString()) || !arrayContains("Finance", books[i].genres)) {
                    a++;
                    continue;
                }
            } else if (!isBlank(genreFilter) && !arrayContains(genreFilter, books[i].genres)) {
                a++;
                continue;
            }

            // definine all the necessary divs for a book
            let $book = $("<div>", {
                "class": "card book"
            });
            let $book_meta = $("<div>", {
                "class": "book-meta"
            });
            let $book_author = $("<div>", {
                "class": "book-author"
            });
            let $book_name = $("<div>", {
                "class": "book-name"
            });
            let $book_publish_date = $("<div>", {
                "class": "book-publish-date"
            });
            let $book_genres = $("<div>", {
                "class": "book-genres"
            });

            //append elements to #book-container
            $book_author.append(books[i].author.name);
            $book_name.append(books[i].name);
            $book_publish_date.append(books[i].published.day + "/" + books[i].published.month + "/" + books[i].published.year)

            for (let j = 0; j < books[i].genres.length; j++) {
                let $book_genre = $("<div>", {
                    "class": "book-genre"
                });
                $book_genre.append(books[i].genres[j]);

                $book_genre.click(function () {
                    loadingAnimation();
                    currentGenreFilter = books[i].genres[j];
                    $("#genre-name").html(books[i].genres[j]);
                    reload();
                })
                $book_genres.append($book_genre);
            }

            //    $book.css('background-image', 'url("https://picsum.photos/200?random")');
            $book.css('background-image', 'url("files/jpg/book-covers-2/' + (Math.floor((Math.random() * 586)) + 1) + '.jpg")');
            if (books[i].published.month == 10 && books[i].published.day == 31 && arrayContains("Horror", books[i].genres)) {
                $book_meta.addClass("book-meta-halloween");
                $book_genres.css('color', 'black');
                $book.css('background-image', 'url("files/jpg/book-covers/helloween.jpg');
            }
            let date = new Date(books[i].published.year, books[i].published.month - 1, books[i].published.day)
            if (lastFriday(books[i].published.year, books[i].published.month) == date.toDateString() && arrayContains("Finance", books[i].genres)) {
                $book_meta.addClass("book-meta-finance-friday");
            }
            $book_meta.append($book_author);
            $book_meta.append($book_name);
            $book_meta.append($book_publish_date);
            $book_meta.append($book_genres);
            $book.append($book_meta);
            $("#book-container").append($book);
        }
        offset = offset + a;
    }

    /**
     *  Calls loadMore with parameters so that it resets to
     *  AMOUNT_TO_LOAD books and to current gender and
     *  genre filters
     */
    function reload() {
        loadMore(AMOUNT_TO_LOAD, currentGenderFilter, currentGenreFilter, true);
    }

    /** 
     * Load normally more books
     */
    function loadRegular() {
        loadMore(AMOUNT_TO_LOAD, currentGenderFilter, currentGenreFilter, false);
    }

    /** 
     * Called when resetting the filters
     */
    function resetClick() {
        loadingAnimation();
        $("#genre-name").html("");
        currentGenderFilter = null;
        currentGenreFilter = null;
        loadMore(AMOUNT_TO_LOAD, null, null, true);
    }

    // I KNOW THE NEXT TWO FUNCTIONS COULD HAVE
    // BEEN A ONE FUNCTION. HOWEVER, THAT
    // FUNCTION WOULD NOT HAVE BEEN SO PRETTY
    // AS THESE TWO

    /** 
     * Called when filtering by male genre (sex)
     */
    function maleFunction() {
        loadingAnimation();
        currentGenderFilter = "M";
        loadMore(AMOUNT_TO_LOAD, currentGenderFilter, currentGenreFilter, true);
    }

    /**
     * Called when filtering by female genre (sex)
     */
    function femaleFunction() {
        loadingAnimation();
        currentGenderFilter = "F";
        loadMore(AMOUNT_TO_LOAD, currentGenderFilter, currentGenreFilter, true);
    }

    /**
     * function executed when a genre is clicked in a dropdown menu
     * 
     * @param {int} i 
     */
    function genreFunction(i) {
        loadingAnimation();
        currentGenreFilter = genres[i];
        reload();
        // $("#genre-name").removeClass("display-none");
        $("#genre-name").html(currentGenreFilter);
    }

    /** sorts the whole books array by book name alphabetically */
    function sortByBookName() {
        startLoading();
        setTimeout(function () {
            books.sort((a, b) => a.name.localeCompare(b.name));
            stopLoading();
            reload();
        }, 500);
    }

    /** sorts the whole books array by author name alphabetically */
    function sortByAuthorName() {
        startLoading();
        setTimeout(function () {
            books.sort((a, b) => a.author.name.localeCompare(b.author.name));
            stopLoading();
           reload();
        }, 500);
    }

    /**
     * Adds one item to navigation bar and binds
     * its click function to it
     * 
     * @param {item} rootElement 
     * @param {string|int} content 
     * @param {function} clickFunction 
     * 
     * @returns binded element
     */
    function addItemToNav(rootElement, content, clickFunction) {
        let $element = $("<li>");
        let $elementSpan = $("<span>");
        $element.append($elementSpan);
        $elementSpan.html(content);

        $element.click(function () {
            if (clickFunction != null) {
                clickFunction();
            }
        });

        rootElement.append($element);
        return $element;
    }

    /** Adds items to navigation bar */
    function addItemsToNav() {
        let $menuItems = $("<ul>");
        let $genreItems = $("<ul>");
        let $genderItems = $("<ul>");

        addItemToNav($menuItems, "Reset Filters", resetClick);

        addItemToNav($genderItems, "Female", femaleFunction);
        addItemToNav($genderItems, "Male", maleFunction);
        let filterByGender = addItemToNav($menuItems, "Filter by Gender", null);
        filterByGender.append($genderItems);

        for (let i = 0; i < genres.length; i++) {
            addItemToNav($genreItems, genres[i], function () {
                return genreFunction(i);
            });
        }
        let filterByGenre = addItemToNav($menuItems, "Filter by Genre", null);
        filterByGenre.append($genreItems);

        addItemToNav($menuItems, "Sort by Book Name", sortByBookName);
        addItemToNav($menuItems, "Sort by Author Name", sortByAuthorName);

        $("#navbar").append($menuItems);
    }


    function loadingAnimation() {
        changeText();
        startLoading();
        setTimeout(function () {
            stopLoading()
        }, 700);
    }

    /** Starts loading bar animation and hides books */
    function startLoading() {
        $("#color-legend").addClass("display-none");
        $("#genre-name").addClass("display-none");
        $("#book-container").addClass("display-none");
        $("#load-more").addClass("display-none");
        $("#loading").removeClass("display-none");
    }

    /** Stops loading bar animation and shows books */
    function stopLoading() {
        $("#color-legend").removeClass("display-none");
       // $("#load-more").on("click", loadRegular);
        $("#load-more").html("Load 20 More");
        endReached = false;
        if (!isBlank(currentGenreFilter)) {
            $("#genre-name").removeClass("display-none");
        }
        $("#book-container").removeClass("display-none");
        $("#load-more").removeClass("display-none");
        $("#loading").addClass("display-none");
    }

    /**
     * Determines whether a string is
     * NULL, undefined or empty
     * 
     * @param {string} str 
     */
    function isBlank(str) {
        return (!str || /^\s*$/.test(str));
    }

    /**
     * Determines whether an item is in an array
     * 
     * @param {any} item 
     * @param {any array} array 
     */
    function arrayContains(item, array) {
        return (array.indexOf(item) > -1);
    }

    /**
     * Helps with random numbers
     * 
     * @param {Number} max 
     */
    function getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

    /**
     * Determines the last friday of the month in a year 
     * 
     * @param {number} year 
     * @param {number} month
     */
    function lastFriday(year, month) {
        let lastDay;
        let i = 0;
        while (true) {
            lastDay = new Date(year, month, i);
            if (lastDay.getDay() === 5) {
                return lastDay.toDateString();
            }
            i -= 1;
        }
    };
});