# Job Application - SpaceSnek

This project is a solution I came up with for SpaceSnek job application.

## Getting Started

Pull or download this project to your local machine.
Start npm's http-server on any port.

There are two versions of the solution with one minor change on each
The one in the folder public uses a big 200MB json file with all of the one million books. The other one, in the public-faster folder uses a smaller json file and loads much faster.

1. to start the process-heavy one type following commands in your terminal
```
cd project-root-folder
http-server
```

2. to start the lighter version type following commands in your terminal
```
cd project-root-folder
http-server ./public-faster
```

## Solution description

My solution implements all of the functionalities suggested in the SpaceSnek Coding Challenge - List

### Book Sorting

Books can be sorted using one of two given methods
1. Sorting by author's name alphabetically (A-Z)
2. Sorting by book name alphabetically (A-Z)

### Filtering

Books can be filtered by book genre or by author's gender.

Both filterings can be done using the navigation bar on top.
Filtering by book genre can also be done when hovering over the book
and pressing one of the genres it contains.

### Special books

Special books have slightly altered design compared to the others

1. Books that were published on Halloween and are of Horror genre are black with orange glow and a special book cover
2. Books that were published on the last friday of any given month are colored yellow

## Built With
* Vanilla HTML and CSS.
* JavaScript with [JQuery](https://jquery.com/)


## Authors

* **Fran Grgić**